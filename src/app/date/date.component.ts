import { Component, OnInit, Input  } from '@angular/core';
import { CommonModule } from '@angular/common';


@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.css']
})
export class DateComponent implements OnInit {
  
  dates  = [
    {"id": 1, "pete": false, "erno" : false, "pauli": false,"osku": false},
    {"id": 2, "pete": false, "erno" : false,"pauli": false,"osku": false},
    {"id": 3, "pete": false, "erno" : false,"pauli": false, "osku": false},
    {"id": 4, "pete": false,  "erno" : false,"pauli": false,"osku": false},
    {"id": 5, "pete": false,  "erno" : false,"pauli": false,"osku": false},
    {"id": 6, "pete": false,  "erno" : false,"pauli": false,"osku": false},
    {"id": 7, "pete": false,  "erno" : false,"pauli": false,"osku": false},
    {"id": 8, "pete": false,  "erno" : false,"pauli": false,"osku": false},
    {"id": 9, "pete": false,  "erno" : false,"pauli": false,"osku": false},
    {"id": 10, "pete": false,  "erno" : false,"pauli": false,"osku": false},
    {"id": 11, "pete": false, "erno" : false, "pauli": false,"osku": false},
    {"id": 12, "pete": false, "erno" : false,"pauli": false,"osku": false},
    {"id": 13, "pete": false, "erno" : false,"pauli": false, "osku": false},
    {"id": 14, "pete": false,  "erno" : false,"pauli": false,"osku": false},
    {"id": 15, "pete": false,  "erno" : false,"pauli": false,"osku": false},
    {"id": 16, "pete": false,  "erno" : false,"pauli": false,"osku": false},
    {"id": 17, "pete": false,  "erno" : false,"pauli": false,"osku": false},
    {"id": 18, "pete": false,  "erno" : false,"pauli": false,"osku": false},
    {"id": 19, "pete": false,  "erno" : false,"pauli": false,"osku": false},
    {"id": 20, "pete": false,  "erno" : false,"pauli": false,"osku": false}
    ];

    @Input() states: object;

  constructor() { }

  ngOnInit() {
  }

objIndex : any;
changePete(pvm){
  this.objIndex = this.dates.findIndex((obj => obj.id == pvm));
  if(this.dates[this.objIndex].pete == false){
  this.dates[this.objIndex].pete = true
  }
  else{
  this.dates[this.objIndex].pete = false
  }
  console.log("After update: ", this.dates[this.objIndex])
  return 
}
changeErno(pvm){
  this.objIndex = this.dates.findIndex((obj => obj.id == pvm));
  if(this.dates[this.objIndex].erno == false){
  this.dates[this.objIndex].erno = true
  }
  else{
  this.dates[this.objIndex].erno = false 
  }
  console.log("After update: ", this.dates[this.objIndex])
  return 
}
changePauli(pvm){
  this.objIndex = this.dates.findIndex((obj => obj.id == pvm));
  if(this.dates[this.objIndex].pauli == false){
    this.dates[this.objIndex].pauli = true
    }
    else{
    this.dates[this.objIndex].pauli = false 
    }
  console.log("After update: ", this.dates[this.objIndex])
  return 
}
changeOsku(pvm){
  this.objIndex = this.dates.findIndex((obj => obj.id == pvm));
  if(this.dates[this.objIndex].osku == false){
    this.dates[this.objIndex].osku = true
    }
    else{
    this.dates[this.objIndex].osku = false 
    }
  console.log("After update: ", this.dates[this.objIndex])
  return 
}

}


